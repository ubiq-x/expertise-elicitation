# Expertise Elicitation

Expertize on the inner workings of a complex system (financial, social, etc.) or a combination thereof is often diffused. Moreover, that knowledge may not always be well codified or even written down. Experts from adjacent and non-adjacent domains might need to be consulted to gain sufficient insight. The present software allows interviewing multiple experts to elicit their opinions on topics of interest to gain that insight.


## Dependencies

### Server

- [Node.js](https://nodejs.org)
   - [express](https://expressjs.com)
   - [express-session](https://github.com/expressjs/session)
   - [pg-promise](https://github.com/vitaly-t/pg-promise)
   - [Axios](https://axios-http.com)
- PostgreSQL server


### Client

- [Axios](https://axios-http.com)
- [jQuery](https://jquery.com)
- [Material Components for the web](https://github.com/material-components/material-components-web)
- [Google Charts](https://developers.google.com/chart)


## Interview UI

This is the primary way this software is utilized to elicit responses from experts. The process begins with a list of *questions* that will be asked. Information on priors and factors of each question offer a quick insight into how much ground has been covered with the current expert and helps to identify questions to be launched next. Question authoring can be done in the interface (subject to access right).

![Interview: Questions list](img/interview-01-questions.png?raw=true)

Each question is associated with a set of *factors* grouped into *factor groups*. Factors withing a group should be exhaustive and mutually exclusive. Factor and factor group authoring can be done in the interface (subject to access right).

![Interview: Factors list](img/interview-02-factors.png?raw=true)

The first thing an expert provides is the answer to the question itself. In the Bayesian statistics sense, their answer becomes the prior probablility distribution over the answer options. The answer can be elicited for one or more *contexts*. In the screencap below, there are two contexts: "Small farmer" and "Large farmer."

![Interview: Prior elicitation](img/interview-03-prior.png?raw=true)

From then on, the expert provides answer to the question assuming each factor in turn is true (bottom part of the screencap below). This elicits conditional probability distributions over the answer options.

![Interview: Scenario elicitation](img/interview-04-scenario.png?raw=true)


## Survey UI

The system also allows collection of responses to surveys. These surveys are typically distributed to large groups of population in an area of interest.

![Interview: Questions list](img/surveys-01-list.png?raw=true)

Multiple types of questions can be asked. Below is an example of a range type of a question.

![Interview: Questions list](img/surveys-02-range.png?raw=true)

An example of a one-of-many questions is shown below.

![Interview: Questions list](img/surveys-03-single-choice-01.png?raw=true)

![Interview: Questions list](img/surveys-03-single-choice-02.png?raw=true)

At any point, survey statistics can be generated and the distributions of responses inspected.

![Interview: Questions list](img/surveys-05-stats.png?raw=true)


## Multilingual Support

The system has multilungual support. Below is an example of Enlish and French translations of a question along with all of its associated contexts and factors. The interface provides immediate visual feedback about values that have been changed and about errors.

![Admin: Translations management](img/admin-01-translation.png?raw=true)


## Admin UI

The system allows for administrative actions to be taken without direct intraction with the database. Below is an example of several users added to the system. Different user classes have different access rights. For example, both Maintainers and Operators can run an interview, but only Maintainers can add, edit, and delete questions and factors which provides a layer of protection for the content.

![Admin: User management](img/admin-02-user-man.png?raw=true)

All data for a given *project* (projects are the lowest-level organizational units) can be exported to a JSON or TSV file. For organizational convenience, projects are further nested within *areas* which themselves are nested within *countries*.


## License

This project is licensed under the [BSD License](LICENSE.md).
