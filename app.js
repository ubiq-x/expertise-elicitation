/*
 * Notes
 *     - SQL queries are not broken into multiple lines which decreases readability but allows for easy copy-paste
 *       into a Postgres client for debugging, refactoring, and extending.  This is an acceptable trade-off because
 *       once finalized a query is unlikely to change unless the database structure changes.
 *
 * TODO
 *     - Move to HTTPS
 *     - Validate arguments server-side additionally to client-side validation already in place.
 */

const secret = require("./lib/secret.js");

const path = require('path');

const pgp = require('pg-promise')();
const db = pgp(secret.dbConnStr);

const express = require('express');
const session = require('express-session');
const app = express();
const port = 9999;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/static', express.static('static', {extensions: ['css', 'html', 'js', 'tff']}));

// Session:
// https://github.com/expressjs/session#readme

// export NODE_ENV=production

const sess = {
  cookie: {
    maxAge: null
  },
  resave: false,
  rolling: true,
  saveUninitialized: true,  // false
  secret: ['j5kcjhab23l0vo1knl']
};
if (app.get('env') === 'production') {
  // app.set('trust proxy', 1);  // trust first proxy
  sess.cookie.secure = true;
}
app.use(session(sess));


// ---------------------------------------------------------------------------------------------------------------------
const eh = msg => console.log('ERROR:', msg);

const exec = async (qry, args, req, res, fnThen=null, fnCatch=eh, doSend=true) => {
  await db.any(qry, args)
    .then(data => {
      if (fnThen !== null) fnThen(data, req, res);
      if (doSend) res.send(data);
    })
    .catch(fnCatch);
};

// ---------------------------------------------------------------------------------------------------------------------
app.post('/login-token/', async (req, res) => {
  let u = await db.any('SELECT id FROM usr WHERE token = $1', [req.body.token], u => u.id);

  if (u.length === 0) {
    req.session.destroy(err => {});
    res.send({id: null});
    return;
  }

  u = u[0];
  await db.none('UPDATE usr SET login_date = now() WHERE id = $1', [u.id]);
  req.session.regenerate(function (err) {
    if (err) return next(err);
    req.session.usr = {id: u.id};
    req.session.save(function (err) {
      if (err) return next(err);
      res.send(u);
    });
  });
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/login-uname/', async (req, res) => {
  let u = await db.any('SELECT id FROM usr WHERE uname = $1 AND pwd = $2', [req.body.uname, req.body.pwd], u => u.id);

  if (u.length === 0) {
    req.session.destroy(err => {});
    res.send({id: null});
    return;
  }

  u = u[0];
  await db.none('UPDATE usr SET login_date = now() WHERE id = $1', [u.id]);
  req.session.regenerate(function (err) {
    if (err) return next(err);
    req.session.usr = {id: u.id};
    req.session.save(function (err) {
      if (err) return next(err);
      res.send(u);
    });
  });
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/logout/', async (req, res) => {
  if (req.session.usr !== undefined) return;

  console.log(req.session);
  req.session.destroy(err => {});
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/usr-gen-token/', async (req, res) => {
  if (req.session.usr !== undefined) return;

  await db.none('UPDATE usr SET token = gen_random_uuid() WHERE id = $1', [req.session.usr.id]);
  res.send({ok: true});
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/', (req, res) => {
  res.redirect(req.baseUrl + '/static/index.html');
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/add-factors/', async (req, res) => {
  db.any('SELECT id FROM usr WHERE usr_role_id = 5 ORDER BY id', [])
    .then(async users => {
      await db.tx(async tx => {
        const v = '{' + Array.from({length: parseInt(req.body.questionOptionsCnt)}, () => 0).join(',') + '}';

        // const grpId = (req.body.factorGrpId !== null
        //   ? req.body.factorGrpId
        //   : await tx.one('INSERT INTO factor_grp (question_id) VALUES ($1) RETURNING id', [req.body.questionId], g => g.id)
        // );

        let grpId = null;

        // Add to an extant factor group:
        if (req.body.factorGrpId !== null) {
          grpId = req.body.factorGrpId;
        }
        // Add a new factor group:
        else {
          grpId = await tx.one('INSERT INTO factor_grp (question_id, name) VALUES ($1, $2) RETURNING id', [req.body.questionId, req.body.factorGrpName], g => g.id);
          await users.map(async u => await tx.none('INSERT INTO factor_grp_params (factor_grp_id, usr_id) VALUES ($1,$2)', [grpId, u.id]));
        }

        const langs = await tx.any('SELECT id FROM lang ORDER BY id');

        const ids = [];
        for (let t of req.body.txt) {
          const id = await tx.one('INSERT INTO factor (question_id, factor_grp_id) VALUES ($1,$2) RETURNING id', [req.body.questionId, grpId], f => f.id);
          await tx.none('INSERT INTO factor_i18n (factor_id, lang_id, txt) VALUES ($1,$2,$3)', [id, req.body.langId, t]);
          for (const l of langs) {  // add for all other languages
            if (l.id === req.body.langId) continue;
            await tx.none('INSERT INTO factor_i18n (factor_id, lang_id, txt) VALUES ($1,$2,$3)', [id, l.id, `TRANSLATE: ${t}`]);
          }
          // await users.map(async u => await tx.none('INSERT INTO factor_params (factor_id, usr_id, w_i) VALUES ($1, $2, $3)', [id, u.id, v]));
          const questionCtx = await tx.any('SELECT id FROM question_ctx WHERE question_id = $1 ORDER BY id', [req.body.questionId]);
          for (const u of users) {
            for (const qc of questionCtx) {
              await tx.none('INSERT INTO factor_params (factor_id, question_ctx_id, usr_id, w_i) VALUES ($1,$2,$3,$4)', [id, qc.id, u.id, v])
            }
          }
          ids.push(id);
        }
        return ids;
      })
      .then(ids => res.redirect(`/get-factor-lst/${ids.join(',')}-${req.body.usrId}-${req.body.langId}`))
      .catch(eh);
    })
    .catch(eh);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/add-question/', async (req, res) => {
  const langId = (req.body.langId !== null ? req.body.langId : 'en');

  db.any('SELECT id FROM usr WHERE usr_role_id = 5 ORDER BY id', [])
    .then(async users => {
      await db.tx(async tx => {
        const langs = await tx.any('SELECT id FROM lang ORDER BY id');

        const prior = '{' + Array.from({length: parseInt(req.body.options.length)}, () => 0).join(',') + '}';
        const options = '{"' + req.body.options.join('","') + '"}';
        const optionsShort = '{"' + req.body.optionsShort.join('","') + '"}';
        const ctxs = req.body.ctxs;

        // Question:
        const id = await tx.one('INSERT INTO question (project_id, ord_no) VALUES ($1, (SELECT COALESCE(MAX(ord_no) + 1, 1) FROM question WHERE project_id = $1)) RETURNING id', [req.body.projectId], q => q.id);
        await tx.none('INSERT INTO question_i18n (question_id, lang_id, txt, options, options_short) VALUES ($1,$2,$3,$4,$5)', [id, req.body.langId, req.body.txt, options, optionsShort]);
        for (const l of langs) {  // add for all other languages
          if (l.id === req.body.langId) continue;
          await tx.none('INSERT INTO question_i18n (question_id, lang_id, txt, options, options_short) VALUES ($1,$2,$3,$4,$5)', [id, l.id, `TRANSLATE: ${req.body.txt}`, options, optionsShort]);
        }

        for (const c of ctxs) {
          // Context:
          const cid = await tx.one('INSERT INTO question_ctx (question_id) VALUES ($1) RETURNING id', [id], c => c.id);
          await tx.none('INSERT INTO question_ctx_i18n (question_ctx_id, lang_id, txt) VALUES ($1,$2,$3)', [cid, langId, c]);
          for (const l of langs) {  // add for all other languages
            if (l.id === req.body.langId) continue;
            await tx.none('INSERT INTO question_ctx_i18n (question_ctx_id, lang_id, txt) VALUES ($1,$2,$3)', [cid, l.id, `TRANSLATE: ${c}`]);
          }

          // Question params:
          await users.map(async u => await tx.none('INSERT INTO question_params (question_id, question_ctx_id, usr_id, prior) VALUES ($1,$2,$3,$4)', [id, cid, u.id, prior]));
        }

        return id;
      })
      .then(id => res.redirect(`/get-question-lst-one/${id}-${req.body.usrId}-${req.body.langId}`))
      .catch(eh);
    })
    .catch(eh);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/add-usr/', async (req, res) => {
  let uIn = {
    id          : null,
    ts          : null,
    usr_role_id : req.body.usr_role_id,
    uname       : req.body.uname.trim(),
    pwd         : req.body.pwd.trim(),
    name        : req.body.name.trim(),
    note        : req.body.note.trim(),
    is_test     : req.body.is_test
  };

  const roleSymbol = await db.one('SELECT symbol FROM usr_role WHERE id = $1', [uIn.usr_role_id], r => r.symbol);

  // Adding a non "expert" user:
  if (roleSymbol !== 'exp') {
    // await res.send(await exec('INSERT INTO usr (usr_role_id, uname, pwd, name, note, is_test) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id, ts', [u.usr_role_id, u.uname, u.pwd, u.name, u.note, u.is_test], req, res)[0]);
    // const uIns = await exec('INSERT INTO usr (usr_role_id, uname, pwd, name, note, is_test) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id, ts', [u.usr_role_id, u.uname, u.pwd, u.name, u.note, u.is_test], req, res, (data, res, req) => { console.log(data); return data[0]; }, eh, false);
    // const uIns = await exec('INSERT INTO usr (usr_role_id, uname, pwd, name, note, is_test) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id, ts', [u.usr_role_id, u.uname, u.pwd, u.name, u.note, u.is_test], req, res, null, eh, false)[0];

    // let uIns = {};
    // await exec('INSERT INTO usr (usr_role_id, uname, pwd, name, note, is_test) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id, ts', [u.usr_role_id, u.uname, u.pwd, u.name, u.note, u.is_test], req, res, async (data, res, req) => { console.log(data); uInt = data[0]; }, eh, false)[0];
    // console.log(uIns);
    // res.send(uIns);

    await db.tx(async tx => {
      return await tx.one('INSERT INTO usr (usr_role_id, uname, pwd, name, note, is_test) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id, ts', [uIn.usr_role_id, uIn.uname, uIn.pwd, uIn.name, uIn.note, uIn.is_test], req, res);
    })
      .then(u => res.send(u))
      .catch(eh);
  }

  // Adding an "expert" user (involes multiple tables):
  else {
    await db.tx(async tx => {
      // User:
      const uOut = await tx.one('INSERT INTO usr (usr_role_id, uname, pwd, name, note, is_test) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id, ts', [uIn.usr_role_id, uIn.uname, uIn.pwd, uIn.name, uIn.note, uIn.is_test], req, res);

      // Question and factor parameters:
      const questions = await tx.any('SELECT id FROM question');
      for (const q of questions) {
        const ctxs = await tx.any('SELECT id FROM question_ctx WHERE question_id = $1', [q.id]);

        // Construct prior:
        const n = await tx.one('SELECT cardinality(prior) AS n FROM question_params WHERE question_id = $1 LIMIT 1', [q.id], qp => qp.n);
        const prior = '{' + Array.from({length: n}, () => 0).join(',') + '}';

        // Question parameters:
        await ctxs.map(async c => tx.none('INSERT INTO question_params (question_id, question_ctx_id, usr_id, prior) VALUES ($1,$2,$3,$4)', [q.id, c.id, uOut.id, prior]));

        // Factor parameters:
        const factors = await tx.any('SELECT id FROM factor WHERE question_id = $1', [q.id]);
        for (const f of factors) {
          await ctxs.map(async c => tx.none('INSERT INTO factor_params (factor_id, question_ctx_id, usr_id, w_i) VALUES ($1,$2,$3,$4)', [f.id, c.id, uOut.id, prior]));
        }

        // Factor group parameters:
        const groups = await tx.any('SELECT id FROM factor_grp WHERE question_id = $1', [q.id]);
        await groups.map(async g => tx.none('INSERT INTO factor_grp_params (factor_grp_id, usr_id) VALUES ($1,$2)', [g.id, uOut.id]));
      }
      return uOut;
    })
      .then(u => res.send(u))
      .catch(eh);
  }
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/del-factor/', async (req, res) => {
  const grpId = await db.one('SELECT factor_grp_id FROM factor WHERE id = $1;', [req.body.factorId], f => f.factor_grp_id);
  const factorCnt = await db.one('SELECT COUNT(*) AS n FROM factor WHERE factor_grp_id = $1;', [grpId], f => f.n);

  // Removing the sole factor group factor:
  if (factorCnt <= 1) exec('DELETE FROM factor_grp WHERE id = $1', [grpId], req, res);

  // Other factors remain in the factor group:
  else exec('DELETE FROM factor WHERE id = $1', [req.body.factorId], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/del-factor-grp/', async (req, res) => {
  exec('DELETE FROM factor_grp WHERE id = $1', [req.body.id], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/del-question/', async (req, res) => {
  const prjId = await db.one('SELECT project_id FROM question WHERE id = $1;', [req.body.id], q => q.project_id);
  const ordNo = await db.one('SELECT ord_no FROM question WHERE id = $1;', [req.body.id], q => q.ord_no);

  exec('BEGIN; UPDATE question SET is_del = TRUE, ord_no = -1 WHERE id = $1; UPDATE question SET ord_no = ord_no - 1 WHERE project_id = $2 AND ord_no > $3; COMMIT;', [req.body.id, prjId, ordNo], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/del-usr/', async (req, res) => {
  exec('UPDATE usr SET is_del = TRUE WHERE id = $1', [req.body.id], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/dup-question/', async (req, res) => {
  // xxx

  await db.tx(async tx => {
    // Question:
    const prjId = await tx.one('SELECT project_id FROM question WHERE id = $1', [req.body.id], q => q.project_id);
    // const qId = await tx.one('INSERT INTO question (project_id, ord_no) SELECT project_id, (SELECT COALESCE(MAX(ord_no) + 1, 1) FROM question WHERE project_id = $1) FROM question WHERE id = $1 RETURNING id', [req.body.id], q => q.id);
    const qId = await tx.one('INSERT INTO question (project_id, ord_no) VALUES ($1, (SELECT COALESCE(MAX(ord_no) + 1, 1) FROM question WHERE project_id = $1)) RETURNING id', [prjId], q => q.id);
    await tx.none('INSERT INTO question_i18n (question_id, lang_id, txt, options, options_short) SELECT $2 AS question_id, lang_id, txt, options, options_short FROM question_i18n WHERE question_id = $1', [req.body.id, qId]);

    // Question contexts:
    // const contexts = await tx.any('SELECT qc.id, qci.txt, qci.lang_id FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1', [req.body.id]);
    const contexts = await tx.any('SELECT id FROM question_ctx qc WHERE question_id = $1', [req.body.id]);
    console.log(contexts);
    for (const c of contexts) {
      await tx.none('INSERT INTO question_ctx_i18n (question_ctx_id, lang_id, txt) SELECT $2 AS question_ctx_id, lang_id, txt FROM question_ctx_i18n WHERE question_ctx_id = $1', [c.id_new, c.lang_id, c.id]);

      // console.log(c);
      c.id = await tx.one('INSERT INTO question_ctx (question_id) VALUES ($1) RETURNING id', [qId], c => c.id);
      // console.log(c);
      tx.none('INSERT INTO question_ctx_i18n (question_ctx_id, lang_id, txt) VALUES ($1,$2,$3)', [c.id, c.lang_id, c.txt]);
    }

    // Question params:
    for (const c of contexts) {
      await tx.none('INSERT INTO question_params (question_id, question_ctx_id, usr_id, prior, is_prior_locked, updated, note) SELECT $2 AS question_id, $3 AS question_ctx_id, usr_id, prior, is_prior_locked, updated, note FROM question_params WHERE question_id = $1', [req.body.id, qId, c.id]);
    }

    // Factor groups:
    const gIdMap = {};  // mapping: old group id --> new group id; needed for duplicating factors later on
    const groups = await tx.any('SELECT id, name FROM factor_grp WHERE question_id = $1', [req.body.id]);
    for (let g of groups) {
      const gId = await tx.one('INSERT INTO factor_grp (question_id, name) VALUES ($1,$2) RETURNING id', [qId, g.name], g => g.id);
      await tx.none('INSERT INTO factor_grp_params (factor_grp_id, usr_id, note) SELECT $2 AS factor_grp_id, usr_id, note FROM factor_grp_params WHERE factor_grp_id = $1', [g.id, gId]);
      gIdMap[g.id] = gId;
    }

    // Factors and factor params:
    const factors = await tx.any('SELECT id, factor_grp_id FROM factor WHERE question_id = $1', [req.body.id]);
    for (const f of factors) {
      // Factor:
      const fId = await tx.one('INSERT INTO factor (question_id, factor_grp_id) VALUES ($1,$2) RETURNING id', [qId, gIdMap[f.factor_grp_id]], f => f.id);
      await tx.none('INSERT INTO factor_i18n (factor_id, lang_id, txt) SELECT $2 AS factor_id, lang_id, txt FROM factor_i18n WHERE factor_id = $1', [f.id, fId]);

      // Factor params:
      for (const c of contexts) {
        await tx.none('INSERT INTO factor_params (factor_id, question_ctx_id, usr_id, l, w_sigma, w_i, imp, note) SELECT $2 AS factor_id, $3 AS question_ctx_id, usr_id, l, w_sigma, w_i, imp, note FROM factor_params WHERE factor_id = $1', [f.id, fId, c.id]);
      }
    }

    return qId;
  })
    .then(id => res.redirect(`/get-question-lst-one/${id}-${req.body.usrId}-${req.body.langId}`))
    .catch(eh);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/exp-data-project/:projectId-:langId-:isJSON', async (req, res) => {
  // https://www.postgresql.org/docs/current/functions-formatting.html

  const langId = (req.params.langId !== null ? req.params.langId : 'en');
  const isJSON = false;  // (!!req.params.isJSON);

  const ret = [['row_type', 'question_id', 'question_txt', 'question_options', 'question_options_short', 'ctx_id', 'ctx_txt', 'factor_id', 'factor_txt', 'expert_id', 'expert_name', 'expert_params', 'expert_note', 'expert_last_upd']];

  const users = await db.any('SELECT id, name FROM usr WHERE usr_role_id = 5 ORDER BY id');

  const questions = await db.any('SELECT q.id, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.project_id = $1 AND qi.lang_id = $2 ORDER BY q.id', [req.params.projectId, langId]);
  for (const q of questions) {
    const contexts = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 AND qci.lang_id = $2 ORDER BY qc.id', [q.id, langId]);
    for (const c of contexts) {
      // Question parameters:
      for (const u of users) {
        const qp = await db.one('SELECT prior, COALESCE(to_char(updated, \'YYYY-MM-DD HH24:MI:SS\'), \'\') AS updated, note FROM question_params WHERE question_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [q.id, c.id, u.id]);
        ret.push(['qp', `${q.id}`, `${q.txt}`, q.options.join(','), q.options_short.join(','), `${c.id}`, `${c.txt}`, '', '', `${u.id}`, `${u.name}`, `${qp.prior}`, `${qp.note === null ? '' : qp.note}`, `${qp.updated === null ? '' : qp.updated}`]);
        // ret.push(['qp', `${q.id}`, `"${q.txt.replace('"', '\\"')}"`, `${c.id}`, `"${c.txt}"`, '', '', `${u.id}`, `${u.name}`, `"${qp.prior}"`, `"${qp.note === null ? '' : qp.note.replace('"', '\\"')}"`, `"${qp.updated === null ? '' : qp.updated}"`]);
      }

      // Factor parameters:
      const factors = await db.any('SELECT f.id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 AND fi.lang_id = $2 ORDER BY f.id', [q.id, langId]);
      for (const f of factors) {
        for (const u of users) {
          const fp = await db.any('SELECT w_i, COALESCE(to_char(updated, \'YYYY-MM-DD HH24:MI:SS\'), \'\') AS updated, note FROM factor_params WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [f.id, c.id, u.id]);
          for (const i_fp of fp) {
            ret.push(['fp', `${q.id}`, `${q.txt}`, q.options.join(','), q.options_short.join(','), `${c.id}`, `${c.txt}`, `${f.id}`, `${f.txt}`, `${u.id}`, `${u.name}`, `${i_fp.w_i}`, `${i_fp.note === null ? '' : i_fp.note}`, `${i_fp.updated === null ? '' : i_fp.updated}`]);
            // ret.push(['fp', `${q.id}`, `"${q.txt.replace('"', '\\"')}"`, `${c.id}`, `"${c.txt.replace('"', '\\"')}"`, `${f.id}`, `"${f.txt.replace('"', '\\"')}"`, `${u.id}`, `${u.name}`, `"${i_fp.w_i}"`, `"${i_fp.note === null ? '' : i_fp.note.replace('"', '\\"')}"`, `"${i_fp.updated === null ? '' : i_fp.updated}"`]);
          }
        }
      }
    }
  }

  // res.set('Content-Type', 'text/plain');
  res.set('Content-Type', 'data/csv');
  res.send(ret.map(i => i.join('\t')).join('\n'));
  // res.send(ret.map(i => i.join(',')).join('\n'));
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/exp-data-question/:questionId-:langId-:isJSON', async (req, res) => {
  // https://www.postgresql.org/docs/current/functions-formatting.html

  const langId = (req.params.langId !== null ? req.params.langId : 'en');
  const isJSON = false;  // (!!req.params.isJSON);

  const ret = [['row_type', 'question_id', 'question_txt', 'question_options', 'question_options_short', 'ctx_id', 'ctx_txt', 'factor_id', 'factor_txt', 'expert_id', 'expert_name', 'expert_params', 'expert_note', 'expert_last_upd']];

  const users = await db.any('SELECT id, name FROM usr WHERE usr_role_id = 5 ORDER BY id');

  const questions = await db.any('SELECT q.id, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.id = $1 AND qi.lang_id = $2 ORDER BY q.id', [req.params.questionId, langId]);
    // TODO: The line above (one one below) is the only difference from the 'exp-data-project' command; use that to make the code more succinct across both commands
  for (const q of questions) {
    const contexts = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 AND qci.lang_id = $2 ORDER BY qc.id', [q.id, langId]);
    for (const c of contexts) {
      // Question parameters:
      for (const u of users) {
        const qp = await db.one('SELECT prior, COALESCE(to_char(updated, \'YYYY-MM-DD HH24:MI:SS\'), \'\') AS updated, note FROM question_params WHERE question_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [q.id, c.id, u.id]);
        ret.push(['qp', `${q.id}`, `${q.txt}`, q.options.join(','), q.options_short.join(','), `${c.id}`, `${c.txt}`, '', '', `${u.id}`, `${u.name}`, `${qp.prior}`, `${qp.note === null ? '' : qp.note}`, `${qp.updated === null ? '' : qp.updated}`]);
        // ret.push(['qp', `${q.id}`, `"${q.txt.replace('"', '\\"')}"`, `${c.id}`, `"${c.txt}"`, '', '', `${u.id}`, `${u.name}`, `"${qp.prior}"`, `"${qp.note === null ? '' : qp.note.replace('"', '\\"')}"`, `"${qp.updated === null ? '' : qp.updated}"`]);
      }

      // Factor parameters:
      const factors = await db.any('SELECT f.id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 AND fi.lang_id = $2 ORDER BY f.id', [q.id, langId]);
      for (const f of factors) {
        for (const u of users) {
          const fp = await db.any('SELECT w_i, COALESCE(to_char(updated, \'YYYY-MM-DD HH24:MI:SS\'), \'\') AS updated, note FROM factor_params WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [f.id, c.id, u.id]);
          for (const i_fp of fp) {
            ret.push(['fp', `${q.id}`, `${q.txt}`, q.options.join(','), q.options_short.join(','), `${c.id}`, `${c.txt}`, `${f.id}`, `${f.txt}`, `${u.id}`, `${u.name}`, `${i_fp.w_i}`, `${i_fp.note === null ? '' : i_fp.note}`, `${i_fp.updated === null ? '' : i_fp.updated}`]);
            // ret.push(['fp', `${q.id}`, `"${q.txt.replace('"', '\\"')}"`, `${c.id}`, `"${c.txt.replace('"', '\\"')}"`, `${f.id}`, `"${f.txt.replace('"', '\\"')}"`, `${u.id}`, `${u.name}`, `"${i_fp.w_i}"`, `"${i_fp.note === null ? '' : i_fp.note.replace('"', '\\"')}"`, `"${i_fp.updated === null ? '' : i_fp.updated}"`]);
          }
        }
      }
    }
  }

  res.set('Content-Type', 'text/plain');
    // TODO: The other difference from the 'exp-data-question' command (at least as it is today)
  // res.set('Content-Type', 'data/csv');
  res.send(ret.map(i => i.join('\t')).join('\n'));
  // res.send(ret.map(i => i.join(',')).join('\n'));
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/gen-fake-pop-resp/', async (req, res) => {
  // TODO: Add a transaction block and switch to async-await style.

  const n = parseInt(req.body.n);  // pop size

  db.any('SELECT COALESCE(MAX(id),1) AS id FROM usr', [])
    .then(data => {
      const usrIdMin = Math.max(data[0].id + 1, 3000);

      db.any('SELECT DISTINCT s.id, cardinality(qp.prior) AS m FROM scenario s INNER JOIN question q ON q.id = s.question_id INNER JOIN question_params qp ON qp.question_id = q.id', [])
        .then(async scenarios => {
          for (let i = 0; i < n; i++) {
            const usrId = usrIdMin + i;

            // db.none('INSERT INTO usr (id, name, pwd, is_expert, is_test) VALUES ($1, $2, $3, FALSE, TRUE)', [usrId, 'user-' + usrId, 'pwd-' + usrId])
            const roleIdResp = await db.one('SELECT id FROM usr_role WHERE symbol = \resp\'', r => r.id);
            db.none('INSERT INTO usr (id, usr_role_id, uname, pwd, name, is_test) VALUES ($1,$2,$3,$4,$5, TRUE)', [usrId, roleIdResp, `usr-${usrId}`, `pwd-${usrId}`, `User ${usrId}`])
              .then(data => {
                for (let s of scenarios) {
                  const resp = [];
                  for (let j = 0; j < s.m; j++) {
                    resp.push(Math.floor(Math.random() * 101));
                  }
                  const sum = resp.reduce((partialSum, a) => partialSum + a, 0);
                  resp.map(i => Math.floor(i / sum));

                  db.none('INSERT INTO scenario_resp (scenario_id, usr_id, response) VALUES ($1,$2,$3)', [s.id, usrId, '{' + resp.join(',') + '}'])
                    .catch(eh);
                }
              })
              .catch(eh);
          }
          res.send()
        })
        .catch(eh);
    })
    .catch(eh);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-country-region-project-lst/', async (req, res) => {
  // TODO: Add try-catch block.

  // const orderBy = (!!req.params.doAlphaOrder ? 'name' : 'id');
  //
  // console.log(!!req.params.doAlphaOrder);
  // console.log(orderBy);

  const countries = await db.any(`SELECT id, name FROM country ORDER BY name`, []);
  for (let c of countries) {
    c.regions = await db.any(`SELECT id, name FROM region WHERE country_id = $1 ORDER BY name`, [c.id]);
    for (let r of c.regions) {
      r.projects = await db.any(`SELECT id, name, is_test FROM project WHERE region_id = $1 ORDER BY name`, [r.id]);
    }
  }
  res.send(countries);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-lang-lst/', (req, res) => {
  exec('SELECT ord_no, id, name FROM lang ORDER BY ord_no ASC', [], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-factor/:id-:usrId-:langId', (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  if (langId === '-') {
    exec('SELECT fi.lang_id, f.id, f.factor_grp_id, fi.txt, fp.l, fp.w_sigma, fp.w_i, fp.imp, fp.note FROM factor f INNER JOIN factor_params fp ON fp.factor_id = f.id INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.id = $1 AND fp.usr_id = $2 ORDER BY fi.lang_id, f.factor_grp_id, f.id ASC', [req.params.id, req.params.usrId], req, res);
  }
  else {
    exec('SELECT fi.lang_id, f.id, f.factor_grp_id, fi.txt, fp.l, fp.w_sigma, fp.w_i, fp.imp, fp.note FROM factor f INNER JOIN factor_params fp ON fp.factor_id = f.id INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.id = $1 AND fp.usr_id = $2 AND fi.lang_id = $3 ORDER BY fi.lang_id, f.factor_grp_id, f.id ASC', [req.params.id, req.params.usrId, langId], req, res);
  }
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-factor-lst/:ids-:usrId-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');
  const ids = req.params.ids.split(',').map(Number);

  if (langId === '-') {
    const factors = await db.any('SELECT DISTINCT fi.lang_id, f.id, fg.factor_grp_id, fi.txt, fp.l, fp.w_sigma, fp.w_i, fp.imp, fp.note FROM factor f INNER JOIN factor_params fp ON fp.factor_id = f.id INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.id IN ($1:csv) AND fp.usr_id = $2 ORDER BY fi.lang_id, f.factor_grp_id, f.id ASC', [ids, req.params.usrId], req, res);
    for (const f of factors) {
      f.ctx = await db.any('SELECT question_ctx_id, l, w_sigma, w_i, imp, note FROM factor_params WHERE factor_id = $1 AND usr_id = $2 ORDER BY question_ctx_id ASC;', [f.id, req.params.usrId]);
    }
    res.send(factors);
  }
  else {
    const factors = await db.any('SELECT DISTINCT fi.lang_id, f.id, f.factor_grp_id, fi.txt, fp.l, fp.w_sigma, fp.w_i, fp.imp, fp.note FROM factor f INNER JOIN factor_params fp ON fp.factor_id = f.id INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.id IN ($1:csv) AND fp.usr_id = $2 AND fi.lang_id = $3 ORDER BY fi.lang_id, f.factor_grp_id, f.id ASC', [ids, req.params.usrId, langId], req, res);
    for (const f of factors) {
      f.ctx = await db.any('SELECT question_ctx_id, l, w_sigma, w_i, imp, note FROM factor_params WHERE factor_id = $1 AND usr_id = $2 ORDER BY question_ctx_id ASC;', [f.id, req.params.usrId]);
    }
    res.send(factors);
  }
});

// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-factor-lst-basic/:grpId-:langId', (req, res) => {
  exec('SELECT f.id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.factor_grp_id = $1 AND fi.lang_id = $2 ORDER BY f.id', [req.params.grpId, req.params.langId], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-factor-grp-lst/:questionId-:usrId', (req, res) => {
  exec('SELECT fg.id, fg.name, fgp.note, COUNT(f.*) AS n_factors FROM factor_grp fg INNER JOIN factor_grp_params fgp ON fgp.factor_grp_id = fg.id INNER JOIN factor f ON f.factor_grp_id = fg.id WHERE fg.question_id = $1 AND fgp.usr_id = $2 GROUP BY fg.id, fg.name, fgp.note ORDER BY fg.id', [req.params.questionId, req.params.usrId], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-factor-grp-lst-basic/:questionId', (req, res) => {
  exec('SELECT fg.id, fg.name, COUNT(f.*) AS n_factors FROM factor_grp fg INNER JOIN factor f ON f.factor_grp_id = fg.id WHERE fg.question_id = $1 GROUP BY fg.id, fg.name ORDER BY fg.id', [req.params.questionId], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-project-name/:id', (req, res) => {
  exec('SELECT name FROM project WHERE id = $1', [req.params.id], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question/:id-:usrId-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  if (langId === '-') {
    const question = await db.one('SELECT qi.lang_id, q.id, q.project_id, q.ord_no, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.id = $1 AND qi.lang_id = $2', [req.params.id, langId]);
    question.ctxLst = await db.any('SELECT qci.lang_id, qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 ORDER BY qci.lang_id, qc.id', [req.params.id, langId]);
    question.ctx = await db.any('SELECT question_ctx_id, prior, is_prior_locked, note FROM question_params WHERE question_id = $1 AND usr_id = $2 ORDER BY question_ctx_id ASC', [req.params.id, req.params.usrId]);
    const factors = await db.any('SELECT fi.lang_id, f.id, f.factor_grp_id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 ORDER BY fi.lang_id, f.factor_grp_id, f.id ASC', [req.params.id]);
    const factorGrps = await db.any('SELECT fg.id, fg.name, fgp.usr_id, fgp.note, COUNT(f.id)::int AS n_factors FROM factor_grp fg INNER JOIN question q ON q.id = fg.question_id INNER JOIN factor_grp_params fgp ON fgp.factor_grp_id = fg.id INNER JOIN factor f ON f.factor_grp_id = fg.id WHERE q.id = $1 AND fgp.usr_id = $2 GROUP BY fg.id, fg.name, fgp.usr_id, fgp.note, fgp.note ORDER BY fg.id ASC', [req.params.id, req.params.usrId]);

    for (const f of factors) {
      f.ctx = await db.any('SELECT question_ctx_id, l, w_sigma, w_i, imp, note FROM factor_params WHERE factor_id = $1 AND usr_id = $2 ORDER BY question_ctx_id ASC;', [f.id, req.params.usrId]);
    }

    res.send({question: question, factors: factors, factorGrps: factorGrps});
  }
  else {
    const question = await db.one('SELECT qi.lang_id, q.id, q.project_id, q.ord_no, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.id = $1 AND qi.lang_id = $2', [req.params.id, langId]);
    question.ctxLst = await db.any('SELECT qci.lang_id, qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 AND qci.lang_id = $2 ORDER BY qci.lang_id, qc.id', [req.params.id, langId]);
    question.ctx = await db.any('SELECT question_ctx_id, prior, is_prior_locked, note FROM question_params WHERE question_id = $1 AND usr_id = $2 ORDER BY question_ctx_id ASC', [req.params.id, req.params.usrId]);
    const factors = await db.any('SELECT fi.lang_id, f.id, f.factor_grp_id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 AND fi.lang_id = $2 ORDER BY fi.lang_id, f.factor_grp_id, f.id ASC', [req.params.id, langId]);
    const factorGrps = await db.any('SELECT fg.id, fg.name, fgp.usr_id, fgp.note, COUNT(f.id)::int AS n_factors FROM factor_grp fg INNER JOIN question q ON q.id = fg.question_id INNER JOIN factor_grp_params fgp ON fgp.factor_grp_id = fg.id INNER JOIN factor f ON f.factor_grp_id = fg.id WHERE q.id = $1 AND fgp.usr_id = $2 GROUP BY fg.id, fg.name, fgp.usr_id, fgp.note, fgp.note ORDER BY fg.id ASC', [req.params.id, req.params.usrId]);

    for (const f of factors) {
      f.ctx = await db.any('SELECT question_ctx_id, l, w_sigma, w_i, imp, note FROM factor_params WHERE factor_id = $1 AND usr_id = $2 ORDER BY question_ctx_id ASC;', [f.id, req.params.usrId]);
    }

    res.send({question: question, factors: factors, factorGrps: factorGrps});
  }
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question-i18n/:id', async (req, res) => {
  const question = await db.any('SELECT q.id, qi.lang_id, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.id = $1 ORDER BY qi.lang_id', [req.params.id]);
  const contexts = await db.any('SELECT qc.id, qci.lang_id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 ORDER BY qc.id', [req.params.id]);
  const factorGrps = await db.any('SELECT id, name FROM factor_grp WHERE question_id = $1 ORDER BY id', [req.params.id]);
  const factors = await db.any('SELECT f.id, f.factor_grp_id, fi.lang_id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 ORDER BY f.id, fi.lang_id', [req.params.id]);
  res.send({question: question, contexts: contexts, factors: factors, factorGrps: factorGrps});
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question-lang/:id-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  const question = await db.one('SELECT q.id, qi.lang_id, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.id = $1 AND qi.lang_id = $2', [req.params.id, langId]);
  const contexts = await db.any('SELECT qc.id, qci.lang_id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 AND qci.lang_id = $2 ORDER BY qc.id', [req.params.id, langId]);
  const factorGrps = await db.any('SELECT id, name FROM factor_grp WHERE question_id = $1 ORDER BY id', [req.params.id]);
  const factors = await db.any('SELECT f.id, f.factor_grp_id, fi.lang_id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 AND fi.lang_id = $2 ORDER BY f.id, fi.lang_id', [req.params.id, langId]);

  res.send({question: question, contexts: contexts, factors: factors, factorGrps: factorGrps});
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question-lang-params/:id-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  const users = await db.any('SELECT id, name FROM usr WHERE usr_role_id = 5 ORDER BY id');
  const question = await db.one('SELECT q.id, qi.lang_id, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.id = $1 AND qi.lang_id = $2', [req.params.id, langId]);
  const contexts = await db.any('SELECT qc.id, qci.lang_id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE qc.question_id = $1 AND qci.lang_id = $2 ORDER BY qc.id', [req.params.id, langId]);
  const factorGrps = await db.any('SELECT id, name FROM factor_grp WHERE question_id = $1 ORDER BY id', [req.params.id]);
  const factors = await db.any('SELECT f.id, f.factor_grp_id, fi.lang_id, fi.txt FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id WHERE f.question_id = $1 AND fi.lang_id = $2 ORDER BY f.id, fi.lang_id', [req.params.id, langId]);

  question.prior = [];

  for (const c of contexts) {
    // Question parameters:
    for (const u of users) {
      const qp = await db.one('SELECT prior, COALESCE(to_char(updated, \'YYYY-MM-DD HH24:MI:SS\'), \'\') AS updated, note FROM question_params WHERE question_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [question.id, c.id, u.id]);
      // params.prior.push(['qp', `${q.id}`, `${q.txt}`, q.options.join(','), q.options_short.join(','), `${c.id}`, `${c.txt}`, '', '', `${u.id}`, `${u.name}`, `${qp.prior}`, `${qp.note === null ? '' : qp.note}`, `${qp.updated === null ? '' : qp.updated}`]);
      question.prior.push({contextId: c.id, usrId: u.id, prior: qp.prior, note: qp.note, updated: qp.updated});
    }

    // Factor parameters:
    for (const f of factors) {
      f.params = [];
      for (const u of users) {
        const fp = await db.any('SELECT w_i, COALESCE(to_char(updated, \'YYYY-MM-DD HH24:MI:SS\'), \'\') AS updated, note FROM factor_params WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [f.id, c.id, u.id]);
        for (const i_fp of fp) {
          // params.factor.push(['fp', `${q.id}`, `${q.txt}`, q.options.join(','), q.options_short.join(','), `${c.id}`, `${c.txt}`, `${f.id}`, `${f.txt}`, `${u.id}`, `${u.name}`, `${i_fp.w_i}`, `${i_fp.note === null ? '' : i_fp.note}`, `${i_fp.updated === null ? '' : i_fp.updated}`]);
          f.params.push({contextId: c.id, usrId: u.id, w_i: i_fp.w_i, note: i_fp.note, updated: i_fp.updated});
        }
      }
    }
  }

  res.send({users: users, question: question, contexts: contexts, factors: factors, factorGrps: factorGrps});
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question-lst/:projectId-:usrId-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  let questions = null;

  if (langId === '-') {
    questions = await db.any(
      'WITH ' +
      '  zero_factors AS (SELECT DISTINCT ON (question_id) question_id, SUM(CASE WHEN sum=0 THEN 1 ELSE 0 END) AS n FROM (SELECT factor_id, (SELECT SUM(w_ii) FROM UNNEST(w_i) w_ii) AS sum FROM factor_params WHERE usr_id = $2) fp INNER JOIN factor f ON f.id = fp.factor_id GROUP BY question_id), ' +
      '  zero_priors AS (SELECT DISTINCT ON (question_id) question_id, sum = 0 AS is_zero_prior FROM (SELECT question_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params WHERE usr_id = $2) qp GROUP BY question_id, sum), ' +
      '  priors AS (SELECT z.question_id, COALESCE(nz.usr_cnt, 0) AS usr_cnt_nz, COALESCE(z.usr_cnt, 0) AS usr_cnt_z FROM ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum = 0 ORDER BY question_id) z ' +
      '    FULL JOIN ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum > 0) nz ON z.question_id = nz.question_id) ' +
      'SELECT DISTINCT qi.lang_id, q.id, q.project_id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, COUNT(DISTINCT f.*) AS n_factor, COUNT(DISTINCT fg.*) AS n_factor_grp, zf.n AS n_factor_z FROM question q ' +
      'INNER JOIN question_params qp ON qp.question_id = q.id ' +
      'INNER JOIN question_i18n qi ON qi.question_id = q.id ' +
      'LEFT JOIN zero_factors zf ON zf.question_id = q.id ' +
      'LEFT JOIN zero_priors zp ON zp.question_id = q.id ' +
      'LEFT JOIN priors p ON p.question_id = q.id ' +
      'LEFT JOIN factor f on f.question_id = q.id ' +
      'LEFT JOIN factor_grp fg ON fg.id = f.factor_grp_id ' +
      'WHERE q.is_del = FALSE AND q.project_id = $1 AND qp.usr_id = $2 ' +
      'GROUP BY qi.lang_id, q.id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.prior, qp.is_prior_locked, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, zf.n ' +
      'ORDER BY qi.lang_id, q.ord_no ASC',
      [req.params.projectId, req.params.usrId]
    );
    for (const q of questions) {
      q.ctx = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE question_id = $1 AND lang_id = $2 ORDER BY qc.id', [q.id, langId]);
    }
  }
  else {
    questions = await db.any(
      'WITH ' +
      '  zero_factors AS (SELECT DISTINCT ON (question_id) question_id, SUM(CASE WHEN sum=0 THEN 1 ELSE 0 END) AS n FROM (SELECT factor_id, (SELECT SUM(w_ii) FROM UNNEST(w_i) w_ii) AS sum FROM factor_params WHERE usr_id = $2) fp INNER JOIN factor f ON f.id = fp.factor_id GROUP BY question_id), ' +
      '  zero_priors AS (SELECT DISTINCT ON (question_id) question_id, sum = 0 AS is_zero_prior FROM (SELECT question_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params WHERE usr_id = $2) qp GROUP BY question_id, sum), ' +
      '  priors AS (SELECT z.question_id, COALESCE(nz.usr_cnt, 0) AS usr_cnt_nz, COALESCE(z.usr_cnt, 0) AS usr_cnt_z FROM ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum = 0 ORDER BY question_id) z ' +
      '    FULL JOIN ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum > 0) nz ON z.question_id = nz.question_id) ' +
      'SELECT DISTINCT qi.lang_id, q.id, q.project_id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, COUNT(DISTINCT f.*) AS n_factor, COUNT(DISTINCT fg.*) AS n_factor_grp, zf.n AS n_factor_z FROM question q ' +
      'INNER JOIN question_params qp ON qp.question_id = q.id ' +
      'INNER JOIN question_i18n qi ON qi.question_id = q.id ' +
      'LEFT JOIN zero_factors zf ON zf.question_id = q.id ' +
      'LEFT JOIN zero_priors zp ON zp.question_id = q.id ' +
      'LEFT JOIN priors p ON p.question_id = q.id ' +
      'LEFT JOIN factor f on f.question_id = q.id ' +
      'LEFT JOIN factor_grp fg ON fg.id = f.factor_grp_id ' +
      'WHERE q.is_del = FALSE AND q.project_id = $1 AND qp.usr_id = $2 AND qi.lang_id = $3 ' +
      'GROUP BY qi.lang_id, q.id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.prior, qp.is_prior_locked, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, zf.n ' +
      'ORDER BY q.ord_no ASC',
      [req.params.projectId, req.params.usrId, langId]
    );
    for (const q of questions) {
      q.ctx = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE question_id = $1 AND lang_id = $2 ORDER BY qc.id', [q.id, langId]);
    }
  }
  res.send(questions);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question-lst-one/:id-:usrId-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  let questions = null;

  if (langId === '-') {
    questions = await db.any(
      'WITH ' +
      '  zero_factors AS (SELECT question_id, SUM(CASE WHEN sum=0 THEN 1 ELSE 0 END) AS n FROM (SELECT factor_id, (SELECT SUM(w_ii) FROM UNNEST(w_i) w_ii) AS sum FROM factor_params WHERE usr_id = $2) fp INNER JOIN factor f ON f.id = fp.factor_id GROUP BY question_id), ' +
      '  zero_priors AS (SELECT question_id, sum = 0 AS is_zero_prior FROM (SELECT question_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params WHERE question_id = $1) qp GROUP BY question_id, sum), ' +
      '  priors AS (SELECT z.question_id, COALESCE(nz.usr_cnt, 0) AS usr_cnt_nz, COALESCE(z.usr_cnt, 0) AS usr_cnt_z FROM ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum = 0) z ' +
      '    FULL JOIN ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum > 0) nz ON z.question_id = nz.question_id) ' +
      'SELECT qi.lang_id, q.id, q.project_id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.prior, qp.is_prior_locked, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, COUNT(f.*) AS n_factor, COUNT(DISTINCT fg.*) AS n_factor_grp, zf.n AS n_factor_z FROM question q ' +
      'INNER JOIN question_params qp ON qp.question_id = q.id ' +
      'INNER JOIN question_i18n qi ON qi.question_id = q.id ' +
      'LEFT JOIN zero_factors zf ON zf.question_id = q.id ' +
      'LEFT JOIN zero_priors zp ON zp.question_id = q.id ' +
      'LEFT JOIN priors p ON p.question_id = q.id ' +
      'LEFT JOIN factor f on f.question_id = q.id ' +
      'LEFT JOIN factor_grp fg ON fg.id = f.factor_grp_id ' +
      'WHERE q.id = $1 AND qp.usr_id = $2 ' +
      'GROUP BY qi.lang_id, q.id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.prior, qp.is_prior_locked, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, zf.n ' +
      'ORDER BY qi.lang_id, q.ord_no ASC',
      [req.params.id, req.params.usrId], req, res
    );
    for (const q of questions) {
      q.ctx = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE question_id = $1 AND lang_id = $2 ORDER BY qc.id', [q.id, langId]);
    }
  }
  else {
    questions = await db.any(
      'WITH ' +
      '  zero_factors AS (SELECT question_id, SUM(CASE WHEN sum=0 THEN 1 ELSE 0 END) AS n FROM (SELECT factor_id, (SELECT SUM(w_ii) FROM UNNEST(w_i) w_ii) AS sum FROM factor_params WHERE usr_id = $2) fp INNER JOIN factor f ON f.id = fp.factor_id GROUP BY question_id), ' +
      '  zero_priors AS (SELECT question_id, sum = 0 AS is_zero_prior FROM (SELECT question_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params WHERE question_id = $1) qp GROUP BY question_id, sum), ' +
      '  priors AS (SELECT z.question_id, COALESCE(nz.usr_cnt, 0) AS usr_cnt_nz, COALESCE(z.usr_cnt, 0) AS usr_cnt_z FROM ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum = 0) z ' +
      '    FULL JOIN ' +
      '    (SELECT question_id, COUNT(*) AS usr_cnt FROM ((SELECT question_id, usr_id, SUM(sum) AS sum FROM (SELECT question_id, usr_id, (SELECT SUM(prior_i) FROM UNNEST(prior) prior_i) AS sum FROM question_params) a GROUP BY question_id, usr_id)) b GROUP BY question_id, sum HAVING sum > 0) nz ON z.question_id = nz.question_id) ' +
      'SELECT qi.lang_id, q.id, q.project_id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.prior, qp.is_prior_locked, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, COUNT(f.*) AS n_factor, COUNT(DISTINCT fg.*) AS n_factor_grp, zf.n AS n_factor_z FROM question q ' +
      'INNER JOIN question_params qp ON qp.question_id = q.id ' +
      'INNER JOIN question_i18n qi ON qi.question_id = q.id ' +
      'LEFT JOIN zero_factors zf ON zf.question_id = q.id ' +
      'LEFT JOIN zero_priors zp ON zp.question_id = q.id ' +
      'LEFT JOIN priors p ON p.question_id = q.id ' +
      'LEFT JOIN factor f on f.question_id = q.id ' +
      'LEFT JOIN factor_grp fg ON fg.id = f.factor_grp_id ' +
      'WHERE q.id = $1 AND qp.usr_id = $2 AND qi.lang_id = $3 ' +
      'GROUP BY qi.lang_id, q.id, q.ord_no, qi.txt, qi.options, qi.options_short, qp.prior, qp.is_prior_locked, qp.note, zp.is_zero_prior, p.usr_cnt_nz, p.usr_cnt_z, zf.n ' +
      'ORDER BY qi.lang_id, q.ord_no ASC',
      [req.params.id, req.params.usrId, langId], req, res
    );
    for (const q of questions) {
      q.ctx = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE question_id = $1 AND lang_id = $2 ORDER BY qc.id', [q.id, langId]);
    }
  }
  res.send(questions);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-question-lst-basic/:projectId-:langId', async (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  const questions = await db.any('SELECT q.id, q.ord_no, qi.txt, qi.options, qi.options_short FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE q.is_del = FALSE AND q.project_id = $1 AND qi.lang_id = $2 ORDER BY q.ord_no', [req.params.projectId, langId]);
  for (const q of questions) {
    q.ctx = await db.any('SELECT qc.id, qci.txt FROM question_ctx qc INNER JOIN question_ctx_i18n qci ON qci.question_ctx_id = qc.id WHERE question_id = $1 AND lang_id = $2 ORDER BY qc.id', [q.id, langId]);
  }
  res.send(questions);
});

// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-scenario/:id-:usrId-:langId', (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  db.one('SELECT q.id, qi.txt, qi.options, qi.options_short, r.response FROM question q INNER JOIN question_i18n qi ON qi.question_id = q.id INNER JOIN scenario s on s.question_id = q.id LEFT JOIN (SELECT scenario_id, response FROM scenario_resp WHERE usr_id = $2) r ON r.scenario_id = s.id WHERE s.id = $1 AND qi.lang_id = $3', [req.params.id, req.params.usrId, langId])
    .then(question => {
	  db.any('SELECT f.id, fi.txt, fr.response FROM factor f INNER JOIN factor_i18n fi ON fi.factor_id = f.id INNER JOIN scenario_factor_xr sf ON sf.factor_id = f.id LEFT JOIN factor_resp fr ON fr.factor_id = f.id WHERE sf.scenario_id = $1 AND fi.lang_id = $2', [req.params.id, langId])
        .then(factors => res.send({question: question, factors: factors}))
        .catch(eh);
    })
	.catch(eh);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-scenario-resp/:id-:langId', (req, res) => {
  exec('SELECT usr_id, response FROM scenario_resp WHERE scenario_id = $1', [req.params.id], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-scenario-lst/:langId', (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  exec('SELECT s.id, s.name, q.id AS question_id, qi.txt, qi.options, qi.options_short FROM scenario s INNER JOIN question q ON q.id = s.question_id INNER JOIN question_i18n qi ON qi.question_id = q.id WHERE qi.lang_id = $1', [langId], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-scenario-resp-lst/', (req, res) => {
  exec('SELECT scenario_id, usr_id, response FROM scenario_resp', [], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-survey/:id', (req, res) => {
  exec('SELECT ss.scenario_id AS id, ss.ord_no, s.name FROM survey_scenario_xr ss INNER JOIN scenario s on s.id = ss.scenario_id WHERE ss.survey_id = $1 ORDER BY ss.ord_no ASC', [req.params.id], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-survey-lst/:usrId', (req, res) => {
  exec(
	'SELECT su.id, su.name, a.n AS n_all, COALESCE(b.n, 0) AS n_resp FROM survey su ' +
	'INNER JOIN (SELECT survey_id, COUNT(*) AS n FROM survey_scenario_xr GROUP BY survey_id) a ON a.survey_id = su.id ' +
	'LEFT JOIN (SELECT ss.survey_id, COUNT(*) AS n FROM survey_scenario_xr ss INNER JOIN scenario_resp sr ON sr.scenario_id = ss.scenario_id WHERE sr.usr_id = $1 GROUP BY ss.survey_id) b ON b.survey_id = su.id',
	[req.params.usrId], req, res
  );
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-ui-txt-lst/:langId', (req, res) => {
  const langId = (req.params.langId !== null ? req.params.langId : 'en');

  exec('SELECT id, txt FROM ui_i18n WHERE lang_id = $1', [langId], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
// app.get('/get-usr-id/:hash', (req, res) => {
//   exec('SELECT id FROM usr WHERE hash = $1', [req.hash], req, res);
// });


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-usr-lst/', (req, res) => {
  exec('SELECT id, ts, usr_role_id, uname, name, note, is_test, is_del FROM usr WHERE is_del = FALSE ORDER BY name', [], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-usr/:id', (req, res) => {
  exec('SELECT id, ts, usr_role_id, uname, name, note, is_test, is_del FROM usr WHERE id = $1 ORDER BY name', [req.params.id], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/get-usr-role-lst/', (req, res) => {
  exec('SELECT id, symbol, name, note FROM usr_role ORDER BY id', [], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.get('/ping', (req, res) => { res.send('pong') });


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-grp-name/', (req, res) => {
  exec('UPDATE factor_grp SET name = $2 WHERE id = $1', [req.body.id, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-grp-param-note/', (req, res) => {
  exec('UPDATE factor_grp_params SET note = $3, updated=now() WHERE factor_grp_id = $1 AND usr_id = $2', [req.body.id, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-param-imp/', (req, res) => {
  exec('UPDATE factor_params SET imp = $4 WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [req.body.id, req.body.questionCtxId, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-param-l/', (req, res) => {
  exec('UPDATE factor_params SET l = $4, updated=now() WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [req.body.id, req.body.questionCtxId, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-param-note/', (req, res) => {
  exec('UPDATE factor_params SET note = $3, updated=now() WHERE factor_id = $1 AND usr_id = $2', [req.body.id, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-param-w_sigma/', (req, res) => {
  exec('UPDATE factor_params SET w_sigma = $4, updated=now() WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [req.body.id, req.body.questionCtxId, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-param-w_i/', (req, res) => {
  exec('UPDATE factor_params SET w_i = $4, updated=now() WHERE factor_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [req.body.id, req.body.questionCtxId, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-factor-response/', (req, res) => {
  exec('INSERT INTO factor_resp (factor_id, usr_id, response) VALUES ($1, $2, $3) ON CONFLICT (factor_id, usr_id) DO UPDATE SET response = $3', [req.body.id, req.body.usrId, req.body.resp], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-i18n-factor-txt/', (req, res) => {
  exec(`UPDATE factor_i18n SET txt = $3 WHERE factor_id = $1 AND lang_id = $2`, [req.body.id, req.body.langId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-i18n-question-txt/', (req, res) => {
  exec(`UPDATE question_i18n SET txt = $3 WHERE question_id = $1 AND lang_id = $2`, [req.body.id, req.body.langId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-i18n-question-options/', (req, res) => {
  const val = '{"' + req.body.val.join('","') + '"}';
  exec(`UPDATE question_i18n SET options = $3 WHERE question_id = $1 AND lang_id = $2`, [req.body.id, req.body.langId, val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-i18n-question-options_short/', (req, res) => {
  const val = '{"' + req.body.val.join('","') + '"}';
  exec(`UPDATE question_i18n SET options_short = $3 WHERE question_id = $1 AND lang_id = $2`, [req.body.id, req.body.langId, val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-i18n-question-ctx-txt/', (req, res) => {
  exec(`UPDATE question_ctx_i18n SET txt = $3 WHERE question_ctx_id = $1 AND lang_id = $2`, [req.body.id, req.body.langId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-questions-order/', async (req, res) => {
  await db.tx(async tx => {
    for (const q of req.body) {
      await tx.none('UPDATE question SET ord_no = $[ord_no] WHERE id = $[id]', q);
    }
  })
    .then(res.send({ok: true}))
    .catch(eh);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-question-param-is_prior_locked/', (req, res) => {
  exec('UPDATE question_params SET is_prior_locked = $4 WHERE question_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [req.body.id, req.body.questionCtxId, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-question-param-prior/', (req, res) => {
  exec('UPDATE question_params SET prior = $4, updated=now() WHERE question_id = $1 AND question_ctx_id = $2 AND usr_id = $3', [req.body.id, req.body.questionCtxId, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-question-param-note/', (req, res) => {
  exec('UPDATE question_params SET note = $3, updated=now() WHERE question_id = $1 AND usr_id = $2', [req.body.id, req.body.usrId, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-scenario-response/', (req, res) => {
  exec('INSERT INTO scenario_resp (scenario_id, usr_id, response) VALUES ($1, $2, $3) ON CONFLICT (scenario_id, usr_id) DO UPDATE SET response = $3', [req.body.id, req.body.usrId, req.body.resp], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-usr-name/', (req, res) => {
  exec('UPDATE usr SET name = $2 WHERE id = $1', [req.body.id, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-usr-note/', (req, res) => {
  exec('UPDATE usr SET note = $2 WHERE id = $1', [req.body.id, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-usr-is_test/', (req, res) => {
  exec('UPDATE usr SET is_test = $2 WHERE id = $1', [req.body.id, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/set-usr-pwd/', (req, res) => {
  exec('UPDATE usr SET pwd = $2 WHERE id = $1', [req.body.id, req.body.val], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/upd-factor/', async (req, res) => {
  exec('UPDATE factor_i18n SET txt = $3 WHERE factor_id = $1 AND lang_id = $2', [req.body.factorId, req.body.langId, req.body.txt], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/upd-question/', async (req, res) => {
  if (req.body.options.length !== req.body.optionsShort.length) return res.send({ok: false, msg: 'The number of options and short options items does not match.'});

  const optionCnt = await db.one('SELECT options FROM question_i18n WHERE question_id = $1 AND lang_id = \'en\'', [req.body.questionId], q => q.options.length);
  if (optionCnt !== req.body.options.length) return res.send({ok: false, msg: 'Number of options cannot be changed for existing question.'});

  const options = '{"' + req.body.options.join('","') + '"}';
  const optionsShort = '{"' + req.body.optionsShort.join('","') + '"}';

  exec('UPDATE question_i18n SET txt = $3, options = $4, options_short = $5 WHERE question_id = $1 AND lang_id = $2', [req.body.questionId, req.body.langId, req.body.txt, options, optionsShort], req, res);
});


// ---------------------------------------------------------------------------------------------------------------------
app.post('/upd-usr/', async (req, res) => {
  const u = {
    id   : req.body.usrId,
    name : req.body.name.trim(),
    pwd  : req.body.pwd.trim(),
    note : req.body.note.trim(),
    test : req.body.test
  };

  if (u.pwd.length === 0) {
    exec('UPDATE usr SET name = $2, note = $3, is_test = $4 WHERE id = $1', [u.id, u.name, u.note, u.test], req, res);
  }
  else {
    exec('UPDATE usr SET name = $2, pwd = $3, note = $4, is_test = $5 WHERE id = $1', [u.id, u.name, u.pwd, u.note, u.test], req, res);
  }
});


// ---------------------------------------------------------------------------------------------------------------------
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
