const $ = q => document.querySelector(q);
const $$ = (tag, id=null, cls=null, html=null, parent=null) => {
  const el = document.createElement(tag);
  if (cls !== null) { el.className = cls };
  if (id !== null) { el.id = id };
  if (html !== null) { el.innerHTML = html };
  if (parent !== null) { parent.appendChild(el) };
  return el;
}

const $h = el => el.style.display = 'none';
const $s = el => el.style.display = 'block';
// const $sa = (el, kv) => { Object.keys(kv).forEach(k => el[k] = kv[k]); }
const $sa = (el, kv) => { Object.keys(kv).forEach(k => el.setAttribute(k, kv[k])); }

const $clsAdd = (el,c) => el.classList.add(c);
const $clsRem = (el,c) => el.classList.remove(c);
const $clsRep = (el,c) => el.classList.replace(c);

const $empty = (el) => { while (el.firstChild) el.removeChild(el.firstChild) };
const $setVis = (el, v) => el.style.visibility = v ? 'visible' : 'hidden';

async function $req(path, payload=null, fnPre=null, fnPost=null) {
  state.app.isBusy = true;
  if (fnPre !== null) fnPre();
  (async (path, payload) => {return (payload === null ? await axios.get(path) : await axios.post(path, payload))})(path, payload)
    .then(resp => { state.app.isBusy = false; if (fnPost !== null) fnPost(resp); })
    .catch(err => console.log(err));
}
